<?php include 'layout/header.php'; ?>
<?php include 'layout/nav.php'; ?>

<!-- Begin: Main Slider -->
<div class="main-slider">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 wow fadeInLeft" data-wow-delay="1s">
                <h1 class="headOne">AFI Consulting is the Primary Company</h1>
                <h2 class="headTwo">But also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity. </h2>
                <ul class="arwOne">
                    <li>
                        <a href="#" class="btnStyle"><span>Sign Up Today!</span></a>
                    </li>
                    <li>
                        <img src="images/arowone.png" class="img-fluid" alt="img">
                    </li>
                </ul>

            </div>
            <div class="col-md-6 wow fadeInRight" data-wow-delay="1.2s">
                <figure><img src="images/slideimg.png" class="img-fluid" alt="img"></figure>
            </div>
        </div>
    </div>
</div>
<!-- END: Main Slider -->

<section class="aboutAfi">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow fadeInUp" data-wow-delay="0.6s">
                <h2 class="headOne">About AFI</h2>
            </div>
            <div class="col-md-4">
                <div class="abtBox wow fadeInLeft" data-wow-delay="0.8s">
                    <figure><img src="images/abticon1.png" class="img-fluid" alt="img"></figure>
                    <h3>Energy Procurement</h3>
                    <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="abtBox wow fadeInUp" data-wow-delay="1s">
                    <figure><img src="images/abticon2.png" class="img-fluid" alt="img"></figure>
                    <h3>Energy Efficiency</h3>
                    <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="abtBox wow fadeInRight" data-wow-delay="1.2s">
                    <figure><img src="images/abticon3.png" class="img-fluid" alt="img"></figure>
                    <h3>Strategy</h3>
                    <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="wxclusiveLeadSec">
    <img src="images/shape.png" alt="" class="shapeTop">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="headOne mt-5 wow fadeInUp" data-wow-delay="0.6s">Consistent High-Quality Exclusive Leads</h2>
                <img src="images/img1.jpg" alt="" class="my-5 wow fadeInUp" data-wow-delay="0.8s">
                <div class="btnArrow wow fadeInUp" data-wow-delay="1s">
                    <a href="#" class="btnStyle"><span>Sign Up Today!</span></a>
                    <img src="images/arowone.png" class="img-fluid" alt="img">
                </div>
            </div>
        </div>
    </div>
    <img src="images/shape.png" alt="" class="shapeBotom">
</section>

<section class="procurementSec py-0">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-lg-6">
                <figure class="procurementCircle wow fadeInLeft" data-wow-delay="0.6s">
                    <img src="images/img2.png" alt="">
                </figure>
            </div>
            <div class="col-lg-5 wow fadeInRight" data-wow-delay="0.8s">
                <h2 class="headOne">Electricity Procurement</h2>
                <h3>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</h3>
                <p>We have over 350 COMMERCIAL Customers, our core specialty is Group Electric Procurement Auctions. This auction process is supported by Mantis Innovations “powered by Mantis” (this should be the link on our site). AFI works with schools, hospitals, Cities and individual businesses who need to save money on electricity and understand how to use it better. The second thing </p>
                <a href="#" class="btnStyle">Contact us</a>
            </div>
        </div>
        <div class="row justify-content-center d-md-block d-none wow fadeInUp" data-wow-delay="1s">
            <div class="col-md-12"><img src="images/link-shape.png" alt="" class="d-block mx-auto"></div>
        </div>
        <div class="row align-items-center justify-content-between flex-row-reverse">
            <div class="col-lg-6">
                <figure class="procurementCircle wow fadeInLeft" data-wow-delay="1.2s">
                    <img src="images/img3.png" alt="">
                </figure>
            </div>
            <div class="col-lg-5 wow fadeInRight" data-wow-delay="1.4s">
                <h2 class="headOne">Gas Procurement</h2>
                <h3>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</h3>
                <p>We have over 350 COMMERCIAL Customers, our core specialty is Group Electric Procurement Auctions. This auction process is supported by Mantis Innovations “powered by Mantis” (this should be the link on our site). AFI works with schools, hospitals, Cities and individual businesses who need to save money on electricity and understand how to use it better. The second thing </p>
                <a href="#" class="btnStyle">Contact us</a>
            </div>
        </div>
        <div class="row justify-content-center d-md-block d-none wow fadeInUp" data-wow-delay="1.6s">
            <div class="col-md-12"><img src="images/link-shape.png" alt="" class="fa-flip-horizontal d-block mx-auto"></div>
        </div>
        <div class="row align-items-center justify-content-between">
            <div class="col-lg-6">
                <figure class="procurementCircle wow fadeInLeft" data-wow-delay="1.8s">
                    <img src="images/img4.png" alt="">
                </figure>
            </div>
            <div class="col-lg-5 wow fadeInRight" data-wow-delay="2s">
                <h2 class="headOne">Reverse Auction</h2>
                <h3>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</h3>
                <p>We have over 350 COMMERCIAL Customers, our core specialty is Group Electric Procurement Auctions. This auction process is supported by Mantis Innovations “powered by Mantis” (this should be the link on our site). AFI works with schools, hospitals, Cities and individual businesses who need to save money on electricity and understand how to use it better. The second thing </p>
                <a href="#" class="btnStyle">Contact us</a>
            </div>
        </div>
        <div class="row justify-content-center d-md-block d-none wow fadeInLeft" data-wow-delay="2.2s">
            <div class="col-md-12"><img src="images/link-shape.png" alt="" class="d-block mx-auto"></div>
        </div>
        <div class="row align-items-center justify-content-between flex-row-reverse">
            <div class="col-lg-6">
                <figure class="procurementCircle wow fadeInLeft" data-wow-delay="2.4s">
                    <img src="images/img5.png" alt="">
                </figure>
            </div>
            <div class="col-lg-5 wow fadeInRight" data-wow-delay="2.6s">
                <h2 class="headOne">Bill Auditiong</h2>
                <h3>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</h3>
                <p>We have over 350 COMMERCIAL Customers, our core specialty is Group Electric Procurement Auctions. This auction process is supported by Mantis Innovations “powered by Mantis” (this should be the link on our site). AFI works with schools, hospitals, Cities and individual businesses who need to save money on electricity and understand how to use it better. The second thing </p>
                <a href="#" class="btnStyle">Contact us</a>
            </div>
        </div>
    </div>
</section>

<section class="coverageSec">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 wow fadeInUp" data-wow-delay="0.4s">
                <h2 class="headOne">Uncover topics and coverage about issues that matter to better facility performance</h2>
            </div>
            <div class="col-lg-4">
                <div class="coverageThumb wow fadeInLeft" data-wow-delay="0.6s">
                    <a href="#"><img src="images/img6.jpg" alt=""><i class="fab fa-youtube"></i></a>
                    <div class="content">
                        <h4>AFI Consulting Rebrands Subsidiary, Fairbanks Energy Services, Under Efficiency Solutions Division</h4>
                        <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="coverageThumb wow fadeInLeft" data-wow-delay="0.8s">
                    <a href="#"><img src="images/img7.jpg" alt=""><i class="fab fa-youtube"></i></a>
                    <div class="content">
                        <h4>AFI Consulting Rebrands Subsidiary, Fairbanks Energy Services, Under Efficiency Solutions Division</h4>
                        <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="coverageThumb wow fadeInLeft" data-wow-delay="1s">
                    <a href="#"><img src="images/img8.jpg" alt=""><i class="fab fa-youtube"></i></a>
                    <div class="content">
                        <h4>AFI Consulting Rebrands Subsidiary, Fairbanks Energy Services, Under Efficiency Solutions Division</h4>
                        <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="rateSection">
    <img src="images/shape.png" alt="" class="shapeTop">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center mb-5 wow fadeInUp" data-wow-delay="0.3s">
                <h2 class="headOne">Rate Analysis.</h2>
                <p>Mine De-regulated Energy Markets For Opportunity</p>
            </div>
            <div class="col-md-6">
                <div class="analysisCard wow fadeInLeft" data-wow-delay="0.5s">
                    <div class="imgWrap"><img src="images/icon1.png" alt=""></div>
                    <div class="content">
                        <h3>Tariff Analysis</h3>
                        <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="analysisCard wow fadeInRight" data-wow-delay="0.9s">
                    <div class="imgWrap"><img src="images/icon2.png" alt=""></div>
                    <div class="content">
                        <h3>Utility Negotiations</h3>
                        <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="analysisCard wow fadeInLeft" data-wow-delay="0.5s">
                    <div class="imgWrap"><img src="images/icon3.png" alt=""></div>
                    <div class="content">
                        <h3>Tax Exemptions</h3>
                        <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="analysisCard wow fadeInRight" data-wow-delay="0.9s">
                    <div class="imgWrap"><img src="images/icon4.png" alt=""></div>
                    <div class="content">
                        <h3>Utilities Management</h3>
                        <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="images/shape.png" alt="" class="shapeBotom">
</section>

<section class="contactSec">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-5 col-md-6 wow fadeInLeft" data-wow-delay="1s">
                <h3 class="headTwo">Need Help? Contact us anytime</h3>
                <h2 class="headOne">Contact us</h2>
                <h3>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity.</h3>
                <p>AFI Consulting is the primary company but also owns Accurate Data Systems. AFI Consulting’s focus is on Texas Electricity. We have over 350 COMMERCIAL Customers, our core specialty is Group Electric Procurement Auctions. This auction process is supported by Mantis Innovations “powered by Mantis” (this should be the link on our site). AFI works with schools, hospitals, </p>
                <div class="mt-5">
                    <img src="images/img9.jpg" alt="">
                    <img src="images/img10.jpg" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <form class="contactForm wow fadeInRight" data-wow-delay="1s">
                    <h3>If you prefer to start the process over email, <span>complete the form below to get started.</span></h3>
                    <p>We have over 350 COMMERCIAL Customers, our core specialty is Group Electric Procurement Auctions. This auction process is supported by Mantis Innovations “powered by Mantis” (this should be the link on our site). AFI works with schools, hospitals. </p>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Contact Name *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Company Name *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Phone *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Esitimated Monthly usage *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Desire Start Date *">
                    </div>
                    <p>We will never share your information.</p>
                    <button class="btnStyle" type="submit">Submit Now</button>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include 'layout/footer.php'; ?>
<?php include 'layout/script.php'; ?>