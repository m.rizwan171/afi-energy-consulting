  <?php $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>


  <header class="wow fadeInDown" data-wow-delay="0.6s">
    <div class="container">
      <nav class="navbar navbar-expand-lg">
        <a class="logo" href="index.php"><img src="images/logo.png" alt="img" class="img-fluid"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="fa fa-bars"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item <?= ($activePage == 'index') ? 'active' : ''; ?>">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item <?= ($activePage == '') ? 'active' : ''; ?>">
              <a class="nav-link" href="#">Services</a>
            </li>

            <li class="nav-item <?= ($activePage == '') ? 'active' : ''; ?>">
              <a class="nav-link" href="#">Business</a>
            </li>
            <li class="nav-item <?= ($activePage == '') ? 'active' : ''; ?>">
              <a class="nav-link" href="#">Residential</a>
            </li>
            <li class="nav-item <?= ($activePage == '') ? 'active' : ''; ?>">
              <a class="nav-link" href="#">About Us</a>
            </li>
            <li class="nav-item <?= ($activePage == '') ? 'active' : ''; ?>">
              <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item <?= ($activePage == '') ? 'active' : ''; ?>">
              <a class="nav-link" href="#">Schedule A Call</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </header>