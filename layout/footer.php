<!-- Begin: Footer -->
<footer>
  <img src="images/img11.png" alt="" class="home">
  <img src="images/shape.png" alt="" class="shapeBotom">
  <div class="container">
    <div class="row">
      <div class="col-md-4 wow fadeInLeft" data-wow-delay="1.2s">
        <a href="index.php"><img src="images/logo.png" alt="" class="footer-logo"></a>
        <ul class="list-unstyled social">
          <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
          <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
          <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
          <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 wow fadeInLeft" data-wow-delay="0.8s">
        <div class="center">
          <ul class="list-unstyled links">
            <li><a href="#">Home</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Business</a></li>
            <li><a href="#">Residential</a></li>
            <li><a href="#">About us</a></li>
            <li><a href="#">contact</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 wow fadeInLeft" data-wow-delay="0.4s">
        <ul class="list-unstyled contInfo">
          <li><a href="tel:+123-456-789-0"><img src="images/io1.png" alt="">+123-456-789-0</a></li>
          <li><a href="mailto:AFIConsulting@gmail.com"><img src="images/io2.png" alt="">AFIConsulting@gmail.com</a></li>
          <li><a href="#"><img src="images/io3.png" alt="">217 AFI Consulting <br>city,blog, street CA <br> 92882
            </a></li>
        </ul>
      </div>
    </div>
    <div class="row copyRight wow fadeInUp" data-wow-delay="1.4s">
      <p>Copyright © 2021 All Rights Reserved.</p>
    </div>
  </div>
</footer>
<!-- END: Footer -->